<html>
    <head>
        <title>Test P1</title>
        <style>
            body{
                background-color: #ededed;
                font-family: Verdana, sans-serif;
            }
            h1{
                text-align: center;
            }
            table{
                background-color: #cbcbcb;
                border: none;
            }
            th{
                background-color: #0094f8;
            }
            .column{
                float: middle;
            }
            .top, .bottom{  
                padding-top: 15px;
                padding-bottom: 30px;
                height: 10px;
            }
            .container{
                width: 100%;
                text-align: center;
                background-color: #cbcbcb;
                height: 64px;
                padding-top: 15px;
            }
            .button{
                border: none;
                color: white;
                padding: 15px 35px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                background-color: #0094f8;
            }
            .nh{
                border: none;
                color: white;
                padding: 15px 35px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                background-color: #0094f8;
            }
            .highlight {
                border: none;
                color: white;
                padding: 15px 35px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                background-color: #0094f8;
            border:1px solid white; 
            box-shadow:0px 0px 3px 3px grey;
            }
        </style>
    </head>
    <body>
    <?php 
    $today = date("l");
    $days=["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    ?>
    <div class="column top"><center>Today is <span style="color:green"><?php echo $today ?></span></center></div>
    <div class="container">
    <?php
    foreach($days as $d){
        if(date("l") ==   $d){
    ?>
        <button id="btn-<?php echo $d; ?>" class="nh" style="background-color:#017c27;" onclick="callday('<?php echo $d ?>');"><?php echo $d ?></button> &nbsp;
    <?php 
        }else{
    ?>   
        <button id="btn-<?php echo $d; ?>" class="nh" onclick="callday('<?php echo $d ?>');"><?php echo $d ?></button> &nbsp;
    <?php
        }
    }
    ?>
    </div>
    <div class="column bottom"><center>
    <p>Selected day is <span id="selected" style="color:blue"> </span></p>
    <script type="text/javascript">
    function callday(d){
    
    var buttons = document.getElementById(`btn-${d}`);
    var buttonsRmv = document.querySelectorAll("button");
    buttons.classList.add('highlight');
    
    buttonsRmv.forEach(function(btn){
           if(btn.id !== `btn-${d}`) {
           	btn.classList.remove('highlight');
           }
        })

    
    document.getElementById("selected").innerHTML= d;
    return d;
}
    </script>
    </body>
</html>